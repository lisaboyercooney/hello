package hello.storytests;

import cucumber.api.java.Before;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;

// Import the Hello stub
import hello.stub.HelloFactory;
import hello.stub.HelloStub;
// Import the WeGood stub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.time.LocalTime;

public class BasicIT {

	String helloResponse = null;
	HelloStub hello = null;
	WeGoodStub wegood = null;

public BasicIT() throws Exception {
	String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
	HelloFactory helloFactory = (HelloFactory)
			(Class.forName(helloFactoryClassName).
			getDeclaredConstructor().newInstance());
	this.hello = helloFactory.createHello();

	String wegoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
	WeGoodFactory weGoodFactory = (WeGoodFactory)
			(Class.forName(wegoodFactoryClassName).
			getDeclaredConstructor().newInstance());
			this.wegood = weGoodFactory.createWeGood();

}

	@Given("the time is {string}")
	public void set_time(String timestr) throws Exception {
		LocalTime localTime = LocalTime.parse(timestr);
		this.wegood.setTime(localTime);
}


	@When("I call HelloWorld")
	public void run_hello_world() throws Exception {

		String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
		HelloFactory factory = (HelloFactory)
				(Class.forName(helloFactoryClassName).getDeclaredConstructor().newInstance());
		HelloStub hello = factory.createHello();
		this.helloResponse = hello.hello();

	}

	@Then("it returns {string}")
	public void it_displays_hello_world(String response) throws Exception {
		assertEquals(response, this.helloResponse);
	}
}
