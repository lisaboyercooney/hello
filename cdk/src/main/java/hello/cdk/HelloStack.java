package hello.cdk;

import software.amazon.awscdk.services.ecs.*;
import software.amazon.awscdk.services.ecs.patterns.*;
import software.amazon.awscdk.services.ec2.Vpc;
import software.amazon.awscdk.core.*;

public class HelloStack extends Stack {

  public HelloStack(final Construct scope, final String id) {
    this(scope, id, null);
  }
  public HelloStack(final Construct scope, final String id, final StackProps props) {
    super(scope, id, props);

    Vpc vpc = Vpc.Builder.create(this, "hello-vpc").maxAzs(2).build();
    Cluster cluster = Cluster.Builder.create(this, "hello-cluster").vpc(vpc).build();
    ApplicationLoadBalancedFargateService.Builder.create(this, "hello-service")
      .cluster(cluster)
      .memoryLimitMiB(2048)
      .cpu(512)
      .desiredCount(1)
      .taskImageOptions(
        ApplicationLoadBalancedTaskImageOptions.builder()
        .image(ContainerImage.fromRegistry("lisaboyercooney/hello"))
        .containerPort(4567)
        .build())
      .publicLoadBalancer(true)
      .build();
  }
}
