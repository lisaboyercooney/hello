package hello;

import static spark.Spark.*;
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;
import java.time.LocalTime;

public class HelloWorld {
  WeGoodStub weGood = null;

  public static void main(String[] args) throws Exception {
    HelloWorld helloWorld = new HelloWorld();
    get("/hello", (req,res) -> helloWorld.sayHi());
  }
  public HelloWorld() throws Exception {
    String weGoodFactoryClassName =
      System.getenv("WEGOOD_FACTORY_CLASS_NAME");
    if (weGoodFactoryClassName == null) {
      throw new Exception("WEGOOD_FACTORY_CLASS_NAME is undefined");
    }
    WeGoodFactory factory = (WeGoodFactory)
      (Class.forName(weGoodFactoryClassName)
      .getDeclaredConstructor().newInstance());
    this.weGood = factory.createWeGood();
  }
  public String sayHi() throws Exception {
    boolean weGoodResponse = weGood.areWeGood();
    if (weGoodResponse) return "Hello world!";
    else return "Goodbye";

  }
  /** For testing only */
  public void setTime(LocalTime time) throws Exception {
    this.weGood.setTime(time);
  }

}
