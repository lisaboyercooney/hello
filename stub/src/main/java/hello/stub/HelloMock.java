package hello.stub;

public class HelloMock implements HelloStub {
    public String hello() throws Exception {
        if (good) return "Hello!";
            else return "Goodbye";
    }

    private boolean good = false;

    public void setState(boolean good) {
      this.good = good;
    }
}
