package hello.stub;

public interface HelloStub {
public String hello() throws Exception;
}
