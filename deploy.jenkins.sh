sudo docker login -u $1 -p $2
sudo docker pull ${IMAGE_NAME}
docker network create --subnet=${SUBNET_ADDR_RANGE} mynet
docker run -d --net mynet --ip ${SERVICE_HOST} --name hello ${IMAGE_NAME}
